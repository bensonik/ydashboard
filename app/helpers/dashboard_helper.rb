module DashboardHelper
  def pct_change(old, current)
    return nil unless old && current

    # avoid division by zero
    return nil if old == 0

    (100.0 * current / old).to_i - 100
  end
end
