require "ydashboard/bug_query"
require "ydashboard/closed_bug_result_importer"

class BugzillaClosedQueryJob < ApplicationJob
  queue_as :default

  def perform(*args)
    Rails.logger.info "Starting #{self.class}"
    query = Ydashboard::BugQuery.new(Ydashboard::BugQuery::CLOSED_BUGS_QUERY)
    csv_data = query.run
    importer = Ydashboard::ClosedBugResultImporter.new(csv_data)
    importer.save!
  ensure
    # this takes quite a lot of time - enqueue itself in 2 hours
    BugzillaClosedQueryJob.set(wait: 2.hours).perform_later
  end
end
