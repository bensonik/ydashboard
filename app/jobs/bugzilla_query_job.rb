require "ydashboard/bug_query"
require "ydashboard/bug_result_parser"

class BugzillaQueryJob < ApplicationJob
  queue_as :default

  def perform(*args)
    Rails.logger.info "Starting #{self.class}"
    query = Ydashboard::BugQuery.new
    csv_data = query.run
    importer = Ydashboard::BugResultImporter.new(csv_data)

    Bug.transaction do
      # there are no callbacks or bindings between objects so
      # we can use faster "delete_all" (instead of "destroy_all")
      Bug.delete_all
      importer.save!
    end

    # save the bug summary
    BugStat.collect_for_today.save
  ensure
    # enqueue itself in 15 minutes
    BugzillaQueryJob.set(wait: 15.minutes).perform_later
  end
end
