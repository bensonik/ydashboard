
require "ydashboard/gh_issues_query"
require "ydashboard/gh_issues_importer"

class GhQueryJob < ApplicationJob
  queue_as :default

  def perform(*args)
    Rails.logger.info "Starting #{self.class}"
    # yast organization
    gh_query = Ydashboard::GhIssuesQuery.new
    import_data(gh_query.run)
    # libyui organization
    gh_query = Ydashboard::GhIssuesQuery.new("libyui")
    import_data(gh_query.run)
  rescue Octokit::TooManyRequests
    Rails.logger.warn "GitHub API rate limit reached, set the GitHub token to increase the limit"
  ensure
    # enqueue itself
    GhQueryJob.set(wait: 1.hour).perform_later
  end

private

  def import_data(data)
    importer = Ydashboard::GhIssuesImporter.new(data)
    importer.save!
  end

end
