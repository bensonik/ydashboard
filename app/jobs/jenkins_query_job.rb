
require "ydashboard/jenkins_query"
require "ydashboard/jenkins_result_importer"

class JenkinsQueryJob < ApplicationJob
  queue_as :default

  EXTERNAL_JOBS = /\A(yast|docker-trigger|patterns-yast|libyui|libstorage)-/

  INTERNAL_JOBS = /\A(autoyast|yast|libyui|libstorage)-/

  def perform(*args)
    Rails.logger.info "Starting #{self.class}"
    external_query = Ydashboard::JenkinsQuery.new(job_pattern: EXTERNAL_JOBS, internal_jenkins: false)
    import_jobs(external_query)

    internal_query = Ydashboard::JenkinsQuery.new(job_pattern: INTERNAL_JOBS, internal_jenkins: true)
    import_jobs(internal_query)
  ensure
    # enqueue itself
    JenkinsQueryJob.set(wait: 15.minutes).perform_later
  end

private

  def import_jobs(query)
    importer = Ydashboard::JenkinsResultImporter.new(query)

    importer.import
  rescue SocketError
    Rails.logger.warn "The server is not available"
  end

end
