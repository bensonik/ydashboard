class CreateBugStats < ActiveRecord::Migration[5.0]
  def change
    create_table :bug_stats do |t|
      t.date :date
      t.integer :p0
      t.integer :p1
      t.integer :p2
      t.integer :p3
      t.integer :p4
      t.integer :p5
      t.integer :fresh_new
      t.integer :confirmed
      t.integer :in_progress
      t.integer :reopened

      t.timestamps
    end
  end
end
