class CreateClosedBugStats < ActiveRecord::Migration[5.0]
  def change
    create_table :closed_bug_stats do |t|
      t.date :date, null: false
      t.integer :fixed, default: 0, null: false
      # plain "invalid" conflicts with AR method
      t.integer :resolved_invalid, default: 0, null: false
      t.integer :wontfix, default: 0, null: false
      t.integer :noresponse, default: 0, null: false
      t.integer :upstream, default: 0, null: false
      t.integer :feature, default: 0, null: false
      t.integer :duplicate, default: 0, null: false
      t.integer :worksforme, default: 0, null: false
      t.integer :moved, default: 0, null: false

      t.timestamps
    end
    add_index :closed_bug_stats, :date
  end
end
