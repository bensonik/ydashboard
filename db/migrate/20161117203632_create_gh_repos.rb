class CreateGhRepos < ActiveRecord::Migration[5.0]
  def change
    create_table :gh_repos do |t|
      t.string :name,  null: false
      t.references :gh_org, foreign_key: true

      t.timestamps
    end
    add_index :gh_repos, :name
  end
end
