
require "net/http"
require "uri"
require "yaml"

require "ydashboard/bugzilla_credentials"

module Ydashboard
  RETRY_LIMIT = 10

  class BugQuery
    OPEN_BUGS_QUERY   = "https://bugzilla.suse.com/buglist.cgi?bug_status=UNCONFIRMED&bug_status=NEW&bug_status=CONFIRMED&bug_status=IN_PROGRESS&bug_status=REOPENED&columnlist=priority%2Cproduct%2Cassigned_to%2Cbug_status%2Cshort_desc%2Cchangeddate&email1=%28%28yast2-maintainers%7Cyast-internal%7Cautoyast-maintainers%29%40suse.de%7C%28jreidinger%7Caschnell%7Cjsrain%7Cgsouza%7Cjlopez%7Clocilka%7Cancor%7Cigonzalezsosa%7Ckanderssen%7Csnwint%7Cschubi%7Cshundhammer%7Clslezak%7Cmvidner%7Cmfilka%7Ccwh%29%40suse.com%29&email2=cwh%40suse.com&email3=jreidinger%40suse.com&emailassigned_to1=1&emailtype1=regexp&emailtype2=substring&emailtype3=substring&human=1&limit=0&query_format=advanced&resolution=---&ctype=csv&human=1"
    CLOSED_BUGS_QUERY = "https://bugzilla.suse.com/buglist.cgi?bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2016-01-01&chfieldto=Now&chfieldvalue=RESOLVED&columnlist=priority%2Cresolution&email1=%28%28yast2-maintainers%7Cyast-internal%7Cautoyast-maintainers%29%40suse.de%7C%28jreidinger%7Caschnell%7Cjsrain%7Cgsouza%7Cjlopez%7Clocilka%7Cancor%7Cigonzalezsosa%7Ckanderssen%7Csnwint%7Cschubi%7Cshundhammer%7Clslezak%7Cmvidner%7Cmfilka%7Ccwh%29%40suse.com%29&emailassigned_to1=1&emailtype1=regexp&f1=resolution&human=1&limit=0&o1=changedafter&query_format=advanced&ctype=csv&human=1"

    attr_reader :query

    def initialize(query = OPEN_BUGS_QUERY)
      @query = query
    end

    def run(local_query = query, limit = RETRY_LIMIT)
      resp = http_get(build_query_url(local_query))
      case resp
        when Net::HTTPOK
          save_result(resp) if ENV["YDASHBOARD_SAVE_BUGZILLA_RESPONSE"]
          return resp.body
        when Net::HTTPRedirection
          if limit > 0
            run(resp['location'], limit - 1)
          else
            raise "Bugzilla query failed, error code: #{resp.code}"
          end
        when Net::HTTPUnauthorized
          raise "Invalid Bugzilla credentials"
        else
          raise "Bugzilla query failed, error code: #{resp.code}"
      end
    end

  private

    def http_get(uri, redirection_count: 5)
      Rails.logger.info "Downloading from bugzilla..."
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = uri.scheme == "https"

      req = Net::HTTP::Get.new(uri.request_uri)
      req.basic_auth(uri.user, uri.password) if uri.user && uri.password

      response = http.request(req)
      Rails.logger.info "Result code: #{response.code}"

      response
    end

    def build_query_url(query)
      uri = URI(query)

      credentials = BugzillaCredentials.load
      if credentials.empty?
        Rails.logger.warn "Empty Bugzilla credentials, only public bugs will be returned"
      else
        uri.user = credentials.username
        uri.password = credentials.password
        # use apibugzilla.suse.com for authenticated requests
        uri.host = "apibugzilla.suse.com"
      end

      uri
    end

    def save_result(response)
      fn = File.join(Rails.root, "tmp", Time.now().strftime("bugs-%F-%H-%M-%S.csv"))
      Rails.logger.info "Saving to #{fn}"
      body = response.body
      # the result already is in UTF-8
      body.force_encoding("UTF-8")
      File.write(fn, response.body)
    end

  end
end






