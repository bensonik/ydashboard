
require "csv"

module Ydashboard
  class BugResultImporter
    attr_reader :data
    
    def initialize(query_result)
      @data = query_result
    end
    
    def save!
      CSV.parse(data, headers: true) do |row|
        values = row.to_hash
        Bug.create!(
          bug_id: values["Bug ID"].to_i,
          # parse the "Px - ..." text 
          priority: values["Priority"][1].to_i,
          product: values["Product"],
          assignee: values["Assignee"],
          status: values["Status"],
          summary: values["Summary"],
          change_date: DateTime.parse(values["Changed"])
        )
      end
    end
  end
end
