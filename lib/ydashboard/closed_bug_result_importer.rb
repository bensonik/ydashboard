
require "csv"

module Ydashboard
  class ClosedBugResultImporter
    attr_reader :data
    
    def initialize(query_result)
      @data = query_result
    end
    
    def save!
      ClosedBugStat.transaction do
        closed_stat = ClosedBugStat.find_or_create_by(date: Date.current)
        closed_stat.reset

        CSV.parse(data, headers: true) do |row|
          values = row.to_hash
          resolution = values["Resolution"]
          
          case resolution
          when "FIXED"
            closed_stat.fixed += 1
          when "INVALID"
            closed_stat.resolved_invalid += 1
          when "WONTFIX"
            closed_stat.wontfix += 1
          when "NORESPONSE"
            closed_stat.noresponse += 1
          when "UPSTREAM"
            closed_stat.upstream += 1
          when "FEATURE"
            closed_stat.feature += 1
          when "DUPLICATE"
            closed_stat.duplicate += 1
          when "WORKSFORME"
            closed_stat.worksforme += 1
          when "MOVED"
            closed_stat.moved += 1
          else
            raise "Unhandled bug resolution: #{resolution}"
          end
        end
        
        closed_stat.save!
      end
    end
  end
end
