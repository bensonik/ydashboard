
require "net/http"
require "uri"
require "yaml"

require "ydashboard/bugzilla_credentials"

module Ydashboard
  class ObsStatusQuery
    attr_reader :project, :internal_obs
    
    INTERNAL_OBS_URL = "https://api.suse.de"
    PUBLIC_OBS_URL = "https://api.opensuse.org"

    def initialize(project, internal_obs = false)
      @project = project
      @internal_obs = internal_obs
    end

    def run
      resp = http_get(build_query_url)
      case resp
        when Net::HTTPOK
          save_result(resp) if ENV["YDASHBOARD_SAVE_OBS_RESPONSE"]
          return resp.body
        when Net::HTTPUnauthorized
          raise "Invalid OBS credentials"
        else
          raise "OBS query failed, error code: #{resp.code}"
      end
    end

  private
  
    def api
      internal_obs ? INTERNAL_OBS_URL : PUBLIC_OBS_URL
    end

    def http_get(uri)
      Rails.logger.info "Downloading from OBS (#{api}) #{project}..."
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = uri.scheme == "https"

      req = Net::HTTP::Get.new(uri.request_uri)
      req.basic_auth(uri.user, uri.password) if uri.user && uri.password

      response = http.request(req)
      Rails.logger.info "Result code: #{response.code}"
      response
    end

    def build_query_url
      uri = URI("#{api}/build/#{project}/_result")
      Rails.logger.info "Downloading from #{uri}"

      credentials = BugzillaCredentials.load
      if !credentials.empty?
        uri.user = credentials.username
        uri.password = credentials.password
      end
      
      uri
    end

    def save_result(response)
      fn = File.join(Rails.root, "tmp", Time.now().strftime("OBS-#{project}-%F-%H-%M-%S.xml"))
      Rails.logger.info "Saving to #{fn}"
      File.write(fn, response.body)
    end

  end
end






